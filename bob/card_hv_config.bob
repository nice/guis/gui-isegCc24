<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>HV module config</name>
  <width>730</width>
  <height>285</height>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle</name>
    <class>TITLE-BAR</class>
    <width>730</width>
    <height>50</height>
    <line_width>0</line_width>
    <background_color>
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LabelTitle</name>
    <class>TITLE</class>
    <text>CAN-Line $(CAN_LINE), Module-ID $(MODULE_ID)</text>
    <x use_class="true">0</x>
    <y use_class="true">0</y>
    <width>450</width>
    <height>50</height>
    <font use_class="true">
      <font name="Header 1" family="Liberation Sans" style="BOLD" size="22.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="Text" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <transparent use_class="true">true</transparent>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>SUBTITLE</class>
    <text>HV module control</text>
    <x>460</x>
    <y>20</y>
    <width>260</width>
    <height>30</height>
    <font>
      <font name="Header 2" family="Liberation Sans" style="BOLD" size="18.0">
      </font>
    </font>
    <foreground_color>
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Module settings</name>
    <x>360</x>
    <y>60</y>
    <width>360</width>
    <height>220</height>
    <foreground_color>
      <color name="TEXT" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <widget type="label" version="2.0.0">
      <name>lblVoltage</name>
      <text>Voltage ramp speed:</text>
      <x>22</x>
      <y>10</y>
      <width>150</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="textentry" version="3.0.0">
      <name>entVoltageRampSpeed</name>
      <pv_name>$(DEV):$(CAN_LINE):$(MODULE_ID):VoltageRampSpeed</pv_name>
      <x>182</x>
      <y>10</y>
      <width>115</width>
      <height>18</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <background_color>
        <color name="RED-TEXT" red="255" green="255" blue="255">
        </color>
      </background_color>
    </widget>
    <widget type="textentry" version="3.0.0">
      <name>entCurrentRampSpeed</name>
      <pv_name>$(DEV):$(CAN_LINE):$(MODULE_ID):CurrentRampSpeed</pv_name>
      <x>182</x>
      <y>105</y>
      <width>115</width>
      <height>18</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <background_color>
        <color name="RED-TEXT" red="255" green="255" blue="255">
        </color>
      </background_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblCurrent</name>
      <text>Current ramp speed:</text>
      <x>22</x>
      <y>106</y>
      <width>150</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblVoltageNominal</name>
      <text>V. limit:</text>
      <x>22</x>
      <y>34</y>
      <width>150</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>txtVoltageLimit</name>
      <pv_name>$(DEV):$(CAN_LINE):$(MODULE_ID):VoltageLimit</pv_name>
      <x>182</x>
      <y>33</y>
      <width>115</width>
      <height>18</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>entVoltageLimitNegative</name>
      <pv_name>$(DEV):$(CAN_LINE):$(MODULE_ID):VoltageLimitNegative</pv_name>
      <x>182</x>
      <y>57</y>
      <width>115</width>
      <height>18</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <wrap_words>false</wrap_words>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblVoltageBounds</name>
      <text>V. limit negative:</text>
      <x>22</x>
      <y>58</y>
      <width>150</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="bool_button" version="2.0.0">
      <name>butDisableRamp</name>
      <pv_name>$(DEV):$(CAN_LINE):$(MODULE_ID):CtrlDisaVRampSpeedLimit</pv_name>
      <x>182</x>
      <y>81</y>
      <width>115</width>
      <height>18</height>
      <off_label>Disabled</off_label>
      <on_label>Enabled</on_label>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="10.0">
        </font>
      </font>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Write PV</description>
        </action>
      </actions>
      <show_confirm_dialog>false</show_confirm_dialog>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblDisableVoltageRampSpeedLimit</name>
      <text>V. ramp speed limit:</text>
      <x>22</x>
      <y>82</y>
      <width>150</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblIsFineAdjustment</name>
      <text>Fine adjustment:</text>
      <x>22</x>
      <y>130</y>
      <width>150</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblIsKillEnable</name>
      <text>Kill status:</text>
      <x>22</x>
      <y>155</y>
      <width>150</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledIsFineAdjustment</name>
      <pv_name>$(DEV):$(CAN_LINE):$(MODULE_ID):isFineAdjustment</pv_name>
      <x>182</x>
      <y>128</y>
      <width>60</width>
      <height>19</height>
      <off_label>Off</off_label>
      <on_label>On</on_label>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="10.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ledIsKillEnable</name>
      <pv_name>$(DEV):$(CAN_LINE):$(MODULE_ID):isKillEnable</pv_name>
      <x>182</x>
      <y>154</y>
      <width>60</width>
      <height>19</height>
      <off_label>Disabled</off_label>
      <on_label>Enabled</on_label>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="10.0">
        </font>
      </font>
      <square>true</square>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="bool_button" version="2.0.0">
      <name>btnSetFineAdjustment</name>
      <pv_name>$(DEV):$(CAN_LINE):$(MODULE_ID):Control:setFineAdjustment</pv_name>
      <x>252</x>
      <y>128</y>
      <width>45</width>
      <height>20</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="10.0">
        </font>
      </font>
      <background_color>
        <color name="Read_Background" red="240" green="240" blue="240">
        </color>
      </background_color>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Write PV</description>
        </action>
      </actions>
      <show_confirm_dialog>false</show_confirm_dialog>
    </widget>
    <widget type="bool_button" version="2.0.0">
      <name>btnSetKillEnable</name>
      <pv_name>$(DEV):$(CAN_LINE):$(MODULE_ID):Control:setKillEnable</pv_name>
      <x>252</x>
      <y>154</y>
      <width>45</width>
      <height>20</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="10.0">
        </font>
      </font>
      <background_color>
        <color name="Read_Background" red="240" green="240" blue="240">
        </color>
      </background_color>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Write PV</description>
        </action>
      </actions>
      <show_confirm_dialog>false</show_confirm_dialog>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Module information</name>
    <x>10</x>
    <y>60</y>
    <width>350</width>
    <height>220</height>
    <foreground_color>
      <color name="TEXT" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <widget type="label" version="2.0.0">
      <name>lblSerialNumber</name>
      <text>Serial number:</text>
      <x>6</x>
      <y>10</y>
      <width>129</width>
      <height>18</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblTemp</name>
      <text>Temperature:</text>
      <x>7</x>
      <y>114</y>
      <width>129</width>
      <height>18</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>txtSerialNumber</name>
      <pv_name>$(DEV):$(CAN_LINE):$(MODULE_ID):SerialNumber</pv_name>
      <x>145</x>
      <y>10</y>
      <width>155</width>
      <height>18</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>txtIFanSpeed</name>
      <pv_name>$(DEV):$(CAN_LINE):$(MODULE_ID):Temperature</pv_name>
      <x>146</x>
      <y>114</y>
      <width>155</width>
      <height>18</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <precision>3</precision>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>txtFirmwareName</name>
      <pv_name>$(DEV):$(CAN_LINE):$(MODULE_ID):FirmwareName</pv_name>
      <x>145</x>
      <y>36</y>
      <width>155</width>
      <height>18</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblFirmwareName</name>
      <text>Firmware name:</text>
      <x>6</x>
      <y>36</y>
      <width>129</width>
      <height>18</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblFirmwareRelease</name>
      <text>Firmware release:</text>
      <x>6</x>
      <y>62</y>
      <width>129</width>
      <height>18</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>txtFirmwareRelease</name>
      <pv_name>$(DEV):$(CAN_LINE):$(MODULE_ID):FirmwareRelease</pv_name>
      <x>145</x>
      <y>62</y>
      <width>155</width>
      <height>18</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>txtArticle</name>
      <pv_name>$(DEV):$(CAN_LINE):$(MODULE_ID):Article</pv_name>
      <x>146</x>
      <y>88</y>
      <width>155</width>
      <height>18</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblFirmwareRelease_1</name>
      <text>Article:</text>
      <x>7</x>
      <y>88</y>
      <width>129</width>
      <height>18</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>txtISampleRate</name>
      <pv_name>$(DEV):$(CAN_LINE):$(MODULE_ID):SampleRate</pv_name>
      <x>146</x>
      <y>140</y>
      <width>155</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblSampleRate</name>
      <text>Sample rate:</text>
      <x>7</x>
      <y>140</y>
      <width>129</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>txtIDigitalFilter</name>
      <pv_name>$(DEV):$(CAN_LINE):$(MODULE_ID):DigitalFilter</pv_name>
      <x>146</x>
      <y>165</y>
      <width>155</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="12.0">
        </font>
      </font>
    </widget>
    <widget type="label" version="2.0.0">
      <name>lblTemp_2</name>
      <text>Digital filter:</text>
      <x>7</x>
      <y>165</y>
      <width>129</width>
      <height>15</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
  </widget>
</display>

# -----------------------------------------------------------------------------
# Jython - CSStudio
# -----------------------------------------------------------------------------
# iSeg CC24 controlled modules interface;
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group
# -----------------------------------------------------------------------------
# WP12 - douglas.bezerra.beniz@esss.se
# -----------------------------------------------------------------------------
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from org.csstudio.display.builder.model import WidgetFactory
from org.csstudio.display.builder.model.properties import WidgetColor, WidgetFont, WidgetFontStyle
from org.csstudio.display.builder.runtime.pv import PVFactory

import os, sys, time

from time import sleep
from array import array
from jarray import zeros

# -----------------------------------------------------------------------------
# iSeg maximum number of channels
# -----------------------------------------------------------------------------
MAX_CHANNELS = 48

# -----------------------------------------------------------------------------
# class objects
# -----------------------------------------------------------------------------
logger = ScriptUtil.getLogger()
# -----------------------------------------------------------------------------
# procedures
# -----------------------------------------------------------------------------
def parsingProcedure():
    # -----------------------------------------------------------------------------
    # This Python script is attached to a display
    # and triggered by loc://initial_trigger2$(DID)(1)
    # to execute once when the display is loaded.
    # -----------------------------------------------------------------------------
    try:
        # -----------------------------------------------------------------------------
        # Input PVs
        # -----------------------------------------------------------------------------
        # $(DEV):$(CAN_LINE):$(MODULE_ID):$(CHANNEL_ID):Status

        # -----------------------------------------------------------------------------
        # Loading number of channels for installed modules from PVs
        # -----------------------------------------------------------------------------
        display         = widget.getDisplayModel()
        old_macros      = display.getEffectiveMacros()
        device          = old_macros.getValue('DEV')
        can_line        = old_macros.getValue('CAN_LINE')
        module_id       = old_macros.getValue('MODULE_ID')
        cardType        = "HV"      # at this step of iSeg CC24 integration only High Voltage module was tested; should be improved;
        nbrOfchannels   = 0 

        # for i in range(8):
        #     moduleStatusPvName = "%s:%s:%s:%d:Status" % (device, can_line, 7, i) # $(DEV):$(CAN_LINE):${MODULE_ID}:Status
        #     #logger.info("moduleStatusPvName: %s" % moduleStatusPvName)
        #     modStatusPv = PVFactory.getPV(moduleStatusPvName)
        #     pvEpicsModStatus = PVUtil.getInt(modStatusPv)
        #     PVFactory.releasePV(modStatusPv)
        # except:
        #     pass

        for chan in range(MAX_CHANNELS):
            channelStatusPvName = "%s:%s:%s:%d:Status" % (device, can_line, module_id, chan)
            #logger.info("channelStatusPvName: %s" % channelStatusPvName)
            try:
                chanStatusPv = PVFactory.getPV(channelStatusPvName)
                pvEpicsChanStatus = PVUtil.getInt(chanStatusPv)
                nbrOfchannels += 1
            except:
                # intended to capture NullPointerException meaning that PV is NOT available
                pass
            finally:
                # to avoid memory leak...
                PVFactory.releasePV(chanStatusPv)

        channels = []
        for i in range(nbrOfchannels):
            channels.append({
                            'NAME'       : "Channel %d" % (i),
                            'CHANNEL_ID' : "%d" % i
                            })

        # -----------------------------------------------------------------------------
        # Create display:
        # -----------------------------------------------------------------------------
        embedded_width  = 10
        embedded_height = 10
        label_pos_x = 0
        label_pos_y = 50
        label_width  = 60
        label_height = 20

        def createStatusLabelInstance():
            label = WidgetFactory.getInstance().getWidgetDescriptor("label").createWidget()
            label.setPropertyValue("name", "lblAlarms")
            label.setPropertyValue("text", "Channels")
            label.setPropertyValue("x", label_pos_x)
            label.setPropertyValue("y", label_pos_y)
            label.setPropertyValue("width", label_width)
            label.setPropertyValue("height", label_height)
            label.setPropertyValue("horizontal_alignment", 1)       # Center
            label.setPropertyValue("vertical_alignment", 1)         # Middle
            # public WidgetFont(final String family, final WidgetFontStyle style, final double size)
            label_font = WidgetFont("Source Sans Pro", WidgetFontStyle.BOLD, 11.0)       # font family="Source Sans Pro" style="BOLD" size="12.0"
            label.setPropertyValue("font", label_font)
            # public WidgetColor(final int red, final int green, final int blue, final int alpha)
            back_color = WidgetColor(210, 210, 210)     # Button_Background; red="210" green="210" blue="210"
            label.setPropertyValue("background_color", back_color)
            return label

        def createStatusInstance(x, y, macros):
            embedded = WidgetFactory.getInstance().getWidgetDescriptor("embedded").createWidget();
            embedded.setPropertyValue("x", x)
            embedded.setPropertyValue("y", y)
            embedded.setPropertyValue("width", embedded_width)
            embedded.setPropertyValue("height", embedded_height)
            embedded.setPropertyValue("resize", "2")
            for macro, value in macros.items():
                embedded.getPropertyValue("macros").add(macro, value)
            embedded.setPropertyValue("file", "channel_status_template.bob")
            return embedded

        def createAlarmInstance(x, y, macros=[]):
            embedded = WidgetFactory.getInstance().getWidgetDescriptor("embedded").createWidget();
            embedded.setPropertyValue("x", x)
            embedded.setPropertyValue("y", y)
            embedded.setPropertyValue("width", 58)
            embedded.setPropertyValue("height", 50)
            if cardType == "HV":
                embedded.setPropertyValue("file", "HV/single_hv_alarms.bob")
            #elif cardType == "LV":
            #    embedded.setPropertyValue("file", "card_lv_alarms.bob")
            return embedded

        # ---------------------------------------------------------------------
        # Creating the label to inform the status is related to "Ćhannels"
        # ---------------------------------------------------------------------
        instance = createStatusLabelInstance()
        display.runtimeChildren().addChild(instance)

        # ---------------------------------------------------------------------
        # Creating the LED components for channel status (On/Off)
        # ---------------------------------------------------------------------
        startX   = 10
        startY   = 70
        columns  = 4

        for i in range(len(channels)):
            x = startX + embedded_width * (i % columns)
            y = startY + embedded_height * (i / columns)
            instance = createStatusInstance(x, y, channels[i])
            display.runtimeChildren().addChild(instance)

        # ---------------------------------------------------------------------
        # Creating the LED components for board alarms
        # ---------------------------------------------------------------------
        startX   = 1
        #logger.info("cardType: %s" % str(cardType))
        if cardType == "HV":
            # only HV board has such useful information
            #logger.info("creating alarm display...")
            span_between = embedded_height * ((len(channels) // columns) + 1)        # use the number of rows adding 1 as a space
            instance = createAlarmInstance(startX, startY + span_between)
            display.runtimeChildren().addChild(instance)

    except Exception as e:
        logger.warning("Error! %s " % str(e))

# -----------------------------------------------------------------------------
# calling the main procedure
# -----------------------------------------------------------------------------
sleep(0.2)              # this was necessary because more than one procedure were being started, probably due to the period of scan of CSStudio thread
parsingProcedure()
# -----------------------------------------------------------------------------
# Jython - CSStudio
# -----------------------------------------------------------------------------
# iSeg CC24 controlled modules interface;
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group
# -----------------------------------------------------------------------------
# WP12 - douglas.bezerra.beniz@esss.se
# -----------------------------------------------------------------------------
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from org.csstudio.display.builder.model import WidgetFactory
from org.csstudio.display.builder.runtime.pv import PVFactory

import os, sys, time

from time import sleep
from array import array
from jarray import zeros

# -----------------------------------------------------------------------------
# iSeg crate has 10 slots for HV/LV boards
# -----------------------------------------------------------------------------
MAX_SLOTS = 10

# -----------------------------------------------------------------------------
# class objects
# -----------------------------------------------------------------------------
logger = ScriptUtil.getLogger()
# -----------------------------------------------------------------------------
# procedures
# -----------------------------------------------------------------------------
def parsingProcedure():
    try:
        # -----------------------------------------------------------------------------
        # Loading number of channels for installed modules from PVs
        # -----------------------------------------------------------------------------
        display         = widget.getDisplayModel()
        old_macros      = display.getEffectiveMacros()
        device          = old_macros.getValue('DEV')
        can_line        = old_macros.getValue('CAN_LINE')
        dev_id          = old_macros.getValue('DEVICE_ID')

        # -----------------------------------------------------------------------------
        # Input PVs
        # -----------------------------------------------------------------------------
        #moduleStatusPvName = "%s:%s:%d:Status" % (device, can_line, 7) # $(DEV):$(CAN_LINE):${MODULE_ID}:Status
        #logger.info("moduleStatusPvName: %s" % moduleStatusPvName)
        #modStatusPv = PVFactory.getPV(moduleStatusPvName)
        #pvEpicsModStatus = PVUtil.getInt(modStatusPv)

        moduleStatus = []
        for module in range(MAX_SLOTS):
            moduleStatusPvName = "%s:%s:%d:Status" % (device, can_line, module) # $(DEV):$(CAN_LINE):${MODULE_ID}:Status
            #logger.info("moduleStatusPvName: %s" % moduleStatusPvName)
            try:
                modStatusPv = PVFactory.getPV(moduleStatusPvName)
                pvEpicsModStatus = PVUtil.getInt(modStatusPv)
                # if the Status PV is up, that slot (module) is connected
                moduleStatus.append(module)
            except:
                # intended to capture NullPointerException meaning that PV is NOT available
                pass
            finally:
                # to avoid memory leak...
                PVFactory.releasePV(modStatusPv)

        # -----------------------------------------------------------------------------
        # Loading installed modules from PVs
        # -----------------------------------------------------------------------------
        # Wiener MPOD has 10 slots for HV/LV boards
        # -----------------------------------------------------------------------------
        # Determine card type
        hvCards = []
        lvCards = []

        # ---------------------------------------------------------------------
        # 
        # ---------------------------------------------------------------------
        for idx in moduleStatus:
            # assuming all modules connected are HV...
            hvCards.append(idx)

        # Filling in all boards
        cards = []

        for i in range(MAX_SLOTS):
            cards.append({
                         'NAME' : "BOARD %d" % (i),
                         'CAN_LINE' : "%d" % int(can_line),
                         'DEVICE_ID' : "%d" % int(dev_id),
                         'MODULE_ID' : "%d" % i
                         })

        # Define widget properties
        def createInstance(x, y, id, macros):
            embedded = WidgetFactory.getInstance().getWidgetDescriptor("embedded").createWidget();
            embedded.setPropertyValue("x", x)
            embedded.setPropertyValue("y", y)
            embedded.setPropertyValue("width", embedded_width)
            embedded.setPropertyValue("height", embedded_height)
            embedded.setPropertyValue("resize", "2")
            for macro, value in macros.items():
                embedded.getPropertyValue("macros").add(macro, value)
            if id in hvCards:
                embedded.setPropertyValue("file", "card_hv.bob")
            elif id in lvCards:
                embedded.setPropertyValue("file", "card_lv.bob")
            else:
                embedded.setPropertyValue("file", "card_none.bob")
            return embedded

        # Create widgets
        embedded_width = 70
        embedded_height = 400
        gap = 1
        startX = 30
        startY = 270
        display = widget.getDisplayModel()
        for i in range(len(cards)):
            x = startX + ( (i) * embedded_width + (i) * gap )
            y = startY
            instance = createInstance(x, y, (i), cards[i])
            display.runtimeChildren().addChild(instance)
    except Exception as e:
        logger.warning("Error! %s " % str(e))

# -----------------------------------------------------------------------------
# calling the main procedure
# -----------------------------------------------------------------------------
sleep(0.2)              # this was necessary because more than one procedure were being started, probably due to the period of scan of CSStudio thread
parsingProcedure()
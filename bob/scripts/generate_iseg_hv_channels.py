# -----------------------------------------------------------------------------
# Jython - CSStudio
# -----------------------------------------------------------------------------
# iSeg CC24 controlled modules interface;
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group
# -----------------------------------------------------------------------------
# WP12 - douglas.bezerra.beniz@esss.se
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# Standard libraries
# -----------------------------------------------------------------------------
import os, sys, time

from time import sleep
from array import array
from jarray import zeros

# -----------------------------------------------------------------------------
# Specific libraries on CS-Studio
# -----------------------------------------------------------------------------
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from org.csstudio.display.builder.model import WidgetFactory
from org.csstudio.display.builder.runtime.pv import PVFactory

# -----------------------------------------------------------------------------
# iSeg crate has 10 slots for HV/LV boards
# -----------------------------------------------------------------------------
MAX_SLOTS    = 10
MAX_CHANNELS = 48

# -----------------------------------------------------------------------------
# class objects
# -----------------------------------------------------------------------------
logger = ScriptUtil.getLogger()
# -----------------------------------------------------------------------------
# procedures
# -----------------------------------------------------------------------------
def parsingProcedure():
    # -----------------------------------------------------------------------------
    # This Python script is attached to a display
    # and triggered by loc://initial_trigger$(DID)(1)
    # to execute once when the display is loaded.
    # -----------------------------------------------------------------------------
    try:
        # -----------------------------------------------------------------------------
        # Input PVs
        # -----------------------------------------------------------------------------
        # $(DEV):$(CAN_LINE):$(MODULE_ID):$(CHANNEL_ID):Status

        # -----------------------------------------------------------------------------
        # Loading number of channels for installed modules from PVs
        # -----------------------------------------------------------------------------
        display         = widget.getDisplayModel()
        old_macros      = display.getEffectiveMacros()
        device          = old_macros.getValue('DEV')
        can_line        = old_macros.getValue('CAN_LINE')
        module_id       = old_macros.getValue('MODULE_ID')
        nbrOfchannels   = 0 

        for chan in range(MAX_CHANNELS):
            channelStatusPvName = "%s:%s:%s:%d:Status" % (device, can_line, module_id, chan)
            logger.info("channelStatusPvName: %s" % channelStatusPvName)
            try:
                chanStatusPv = PVFactory.getPV(channelStatusPvName)
                pvEpicsChanStatus = PVUtil.getInt(chanStatusPv)
                nbrOfchannels += 1
            except:
                # intended to capture NullPointerException meaning that PV is NOT available
                pass
            finally:
                # to avoid memory leak...
                PVFactory.releasePV(chanStatusPv)
        #logger.info("nbrOfchannels: %d" % nbrOfchannels)

        channels = []
        for channel in range(nbrOfchannels):
            channels.append({
                            'NAME' : "Channel %d" % (channel),
                            'CHANNEL_ID' : "%d" % channel
                            })

        # -----------------------------------------------------------------------------
        # Create display:
        # -----------------------------------------------------------------------------
        # For each 'channel', add one embedded display
        # which then links to the single_channel_template.bob
        # with the macros of the device.
        embedded_width  = 760
        embedded_height = 20

        def createInstance(x, y, macros):
            embedded = WidgetFactory.getInstance().getWidgetDescriptor("embedded").createWidget();
            embedded.setPropertyValue("x", x)
            embedded.setPropertyValue("y", y)
            embedded.setPropertyValue("width", embedded_width)
            embedded.setPropertyValue("height", embedded_height)
            embedded.setPropertyValue("resize", "2")
            for macro, value in macros.items():
                embedded.getPropertyValue("macros").add(macro, value)
            embedded.setPropertyValue("file", "single_channel_template.bob")
            return embedded

        display = widget.getDisplayModel()
        # coordinates of where the headers stop
        startX = 5
        startY = 110
        # resolution of display
        resX = 760
        resY = 600
        for i in range(len(channels)):
            x = startX
            y = startY + (embedded_height * (i))
            instance = createInstance(x, y, channels[i])
            display.runtimeChildren().addChild(instance)
    except Exception as e:
        logger.warning("Error! %s " % str(e))

# -----------------------------------------------------------------------------
# calling the main procedure
# -----------------------------------------------------------------------------
sleep(0.2)              # this was necessary because more than one procedure were being started, probably due to the period of scan of CSStudio thread
parsingProcedure()
